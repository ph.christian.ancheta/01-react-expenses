# Expenses - React App
A simple React App that allows users to add expenses with a name, amount, and date attribute. Users can also filter in terms of viewing a specific expense

## Setup

```
npm install
npm run start
```
